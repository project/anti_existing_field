<?php

/**
 * @file
 * Disable the ability to re-use fields. Prevents fields
 * from being split out of their original content type tables
 * into separate field-specific tables.
 */

/**
 * Implements hook_menu().
 */
function anti_existing_field_menu() {
  $items = array();
  $items['admin/config/user-interface/anti_existing_field'] = array(
    'title' => 'Anti Existing Field',
    'description' => 'Turn off the ability to re-use fields in CCK.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('anti_existing_field_admin_settings'),
    'access callback' => 'user_access',
    'access arguments' => array('administer site configuration'),
  );

  return $items;
}

/**
 * Settings form.
 *
 * @return array
 */
function anti_existing_field_admin_settings() {
  $form = array();

  $form['anti_existing_field'] = array(
    '#type' => 'radios',
    '#options' => array(
      'off' => t('Turn off re-use interference'),
      'disable' => t('Disable submit button'),
      'remove' => t('Remove add existing field form completely'),
      'prank' => t('Move the submit button on mouseover'),
    ),
    '#default_value' => variable_get('anti_existing_field', 'disable'),
  );

  return system_settings_form($form);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function anti_existing_field_form_field_ui_field_overview_form_alter(&$form, &$form_state, $form_id) {
  switch (variable_get('anti_existing_field', 'disable')) {
    case 'disable':
      $form['_add_existing_field']['label']['#disabled'] = TRUE;
      $form['_add_existing_field']['field_name']['#disabled'] = TRUE;
      $form['_add_existing_field']['type']['#disabled'] = TRUE;
      break;
    case 'remove':
      // don't unset everything because cck/theme/theme.inc will throw notices when it expects existing options to be there
      unset($form['_add_existing_field']['label']);
      unset($form['_add_existing_field']['field_name']);
      unset($form['_add_existing_field']['type']);
      unset($form['_add_existing_field']['widget_type']);
      $form['_add_existing_field']['label'] = array(
        '#value' => l(t('Re-enable this option'), 'admin/config/user-interface/anti_existing_field'),
      );
      break;
    case 'prank':
      $form['_add_existing_field']['label']['#prefix'] = '<div id="anti-existing-field">';
      $form['_add_existing_field']['label']['#suffix'] = '</div>';
      $form['_add_existing_field']['label']['#attributes'] = array('onMouseover' => 'aef_prank()');
      drupal_add_js(drupal_get_path('module', 'anti_existing_field') . '/anti_existing_field.js');
      break;
  }
}
